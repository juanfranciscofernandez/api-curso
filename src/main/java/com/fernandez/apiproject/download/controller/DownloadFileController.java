package com.fernandez.apiproject.download.controller;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.fernandez.apiproject.download.FileInfo;
import com.fernandez.apiproject.download.service.FileStorage;

@RestController
public class DownloadFileController {

	private static final Log logger = LogFactory.getLog(DownloadFileController.class);

	@Autowired
	private FileStorage fileStorage;

	/*
	 * Retrieve Files' Information
	 */
	@GetMapping("/api/public/files")
	public List<FileInfo> getListFiles() {
		List<FileInfo> fileInfos = fileStorage.loadFiles().map(path -> {
			String filename = path.getFileName().toString();
			String url = MvcUriComponentsBuilder
					.fromMethodName(DownloadFileController.class, "downloadFile", path.getFileName().toString()).build()
					.toString();
			return new FileInfo(filename, url);
		}).collect(Collectors.toList());

		return fileInfos;
	}

	/*** Download file ***/
	@GetMapping("/api/public/files/{nombreFoto:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String nombreFoto) {

		Path rutaArchivo = Paths.get("filestorage").resolve(nombreFoto).toAbsolutePath();

		Resource recurso = null;

		try {
			recurso = new UrlResource(rutaArchivo.toUri());

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		if (!recurso.exists() && !recurso.isReadable()) {
			throw new RuntimeException("Error no se pudo cargar la foto: " + nombreFoto);
		}

		HttpHeaders cabecera = new HttpHeaders();
		cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");

		return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
	}
	
	

}
