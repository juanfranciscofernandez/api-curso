package com.fernandez.apiproject.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "language")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Language implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "language_id")
	private long languageId;
	
	@Column(name = "clave")
	private String clave;

	@Column(name = "description")
	private String description;	
		
	@Column(name = "iso639_1")
	private String iso639_1;
	
	@Column(name = "iso639_2")
	private String iso639_2;

	@Column(name = "name")
	private String name;
	
	@Column(name = "native_name")
	private String nativeName;			
	
	
	public Language() {
		super();
	}

	public Language(String iso639_1, String iso639_2, String name) {
		this.iso639_1 = iso639_1;
		this.iso639_2 = iso639_2;
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((iso639_1 == null) ? 0 : iso639_1.hashCode());
		result = prime * result + ((iso639_2 == null) ? 0 : iso639_2.hashCode());
		result = prime * result + (int) (languageId ^ (languageId >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nativeName == null) ? 0 : nativeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Language other = (Language) obj;
		if (clave == null) {
			if (other.clave != null)
				return false;
		} else if (!clave.equals(other.clave))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (iso639_1 == null) {
			if (other.iso639_1 != null)
				return false;
		} else if (!iso639_1.equals(other.iso639_1))
			return false;
		if (iso639_2 == null) {
			if (other.iso639_2 != null)
				return false;
		} else if (!iso639_2.equals(other.iso639_2))
			return false;
		if (languageId != other.languageId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nativeName == null) {
			if (other.nativeName != null)
				return false;
		} else if (!nativeName.equals(other.nativeName))
			return false;
		return true;
	}

	public long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIso639_1() {
		return iso639_1;
	}

	public void setIso639_1(String iso639_1) {
		this.iso639_1 = iso639_1;
	}

	public String getIso639_2() {
		return iso639_2;
	}

	public void setIso639_2(String iso639_2) {
		this.iso639_2 = iso639_2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNativeName() {
		return nativeName;
	}

	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Language [languageId=");
		builder.append(languageId);
		builder.append(", clave=");
		builder.append(clave);
		builder.append(", description=");
		builder.append(description);
		builder.append(", iso639_1=");
		builder.append(iso639_1);
		builder.append(", iso639_2=");
		builder.append(iso639_2);
		builder.append(", name=");
		builder.append(name);
		builder.append(", nativeName=");
		builder.append(nativeName);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
}
