package com.fernandez.apiproject.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sporttranslation")
public class SportTranslation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sportstranslation_id")
	private Long idSportTranslation;

	@Column(name = "sport_id")
	private Long sport;

	@Column(name = "language_id")
	private Long language;

	@Column(name = "name")
	private String name;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSportTranslation == null) ? 0 : idSportTranslation.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((sport == null) ? 0 : sport.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SportTranslation other = (SportTranslation) obj;
		if (idSportTranslation == null) {
			if (other.idSportTranslation != null)
				return false;
		} else if (!idSportTranslation.equals(other.idSportTranslation))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sport == null) {
			if (other.sport != null)
				return false;
		} else if (!sport.equals(other.sport))
			return false;
		return true;
	}

	public Long getIdSportTranslation() {
		return idSportTranslation;
	}

	public void setIdSportTranslation(Long idSportTranslation) {
		this.idSportTranslation = idSportTranslation;
	}

	public Long getSport() {
		return sport;
	}

	public void setSport(Long sport) {
		this.sport = sport;
	}

	public Long getLanguage() {
		return language;
	}

	public void setLanguage(Long language) {
		this.language = language;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "SportTranslation [idSportTranslation=" + idSportTranslation + ", sport=" + sport + ", language="
				+ language + ", name=" + name + "]";
	}

}
