package com.fernandez.apiproject.utils.label;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fernandez.apiproject.model.Label;

@Repository
public interface LabelRepository extends JpaRepository<Label, Long> {
	@Query(value = "SELECT * FROM labels where labels.value_key=:label AND labels.language_id=:language", nativeQuery = true)
	Label findByKey(@Param("label") String label,@Param("language") long language);
	
}
  