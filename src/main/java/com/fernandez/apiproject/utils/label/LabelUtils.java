package com.fernandez.apiproject.utils.label;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LabelUtils {
	
	@Autowired
	private LabelService labelService;
	
	public LabelDTO retreieveLabelServiceByKey(String key) {
		return labelService.getLabelByKey(key);
	}
}
