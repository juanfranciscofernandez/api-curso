package com.fernandez.apiproject.utils.label;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.apiproject.model.Label;
import com.fernandez.apiproject.utils.language.LanguageUtils;

@Component
public class LabelService {

	@Autowired
	private LabelRepository labelRepository;

	@Autowired
	private LabelAdapter labelAdapter;

	@Autowired
	private LanguageUtils languageUtils;

	public LabelDTO getLabelByKey(String key) {
		Label label = labelRepository.findByKey(key, languageUtils.getLanguage());
		return labelAdapter.convert2DTO(label);
	}
}
