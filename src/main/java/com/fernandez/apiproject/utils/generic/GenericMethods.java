package com.fernandez.apiproject.utils.generic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

@Component
public class GenericMethods {
	
	public long toLong(int i) {
		long l;
		if (i < 0) {
			l = -Integer.toUnsignedLong(Math.abs(i));
		} else {
			l = Integer.toUnsignedLong(i);
		}
		return l;
	}
	
	public int toEntero(long i) {
		return (int) i;
	}
	
	
	public Locale stringToLocale(String language) {
		return Locale.forLanguageTag(language);
	}
	
	public String convertToNewFormat(String dateStr) throws ParseException {
	    TimeZone utc = TimeZone.getTimeZone("UTC");
	    SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    sourceFormat.setTimeZone(utc);
	    Date convertedDate = sourceFormat.parse(dateStr);
	    return destFormat.format(convertedDate);
	}
}
