package com.fernandez.apiproject.utils.language;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fernandez.apiproject.language.LanguageDTO;
import com.fernandez.apiproject.language.service.LanguageService;
import com.fernandez.apiproject.model.Language;

@Component
public class LanguageUtils {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private LanguageService languageService;

	public Long getLanguage() {
		
		final String uri = "http://localhost:8688/api/public/v1/language-key/" + getAcceptLanguage();
		RestTemplate restTemplate = new RestTemplate();
		Long result = restTemplate.getForObject(uri, Long.class);
		return result;
	}

	public List<LanguageDTO> getLanguages() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<LanguageDTO>> response = restTemplate.exchange("http://localhost:8584/api/public/v1/languages", HttpMethod.GET, null, new ParameterizedTypeReference<List<LanguageDTO>>() {});
		List<LanguageDTO> languages = response.getBody();
		return languages;
	}

	private String getAcceptLanguage() {
		String language = request.getHeader("Accept-Language");
		Language languageRepo = languageService.findLanguageByKey(language);
		if (languageRepo == null) {
			language = "en-US";
		}
		return language;
	}

}
