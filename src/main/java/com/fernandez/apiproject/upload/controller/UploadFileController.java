package com.fernandez.apiproject.upload.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fernandez.apiproject.download.FileInfo;
import com.fernandez.apiproject.download.service.FileStorage;

@RestController
public class UploadFileController {

	@Autowired
	private FileStorage fileStorage;

	@PostMapping("/api/public/v1/upload")
	public String uploadMultipartFile(@RequestParam("uploadfile") MultipartFile file) {
		try {
			fileStorage.store(file);
			return "File uploaded successfully! -> filename = " + file.getOriginalFilename();
		} catch (Exception e) {
			return "Fail! -> uploaded filename: " + file.getOriginalFilename();
		}
	}

	@PostMapping(value = "/api/public/v1/multipleSave")
	public @ResponseBody String multipleSave(@RequestParam("file") MultipartFile[] files) {
		String msg = "";
		if (files != null && files.length > 0) {
			for (MultipartFile multipartFile : files) {
				try {
					fileStorage.store(multipartFile);
					msg += "You have successfully uploaded " + multipartFile.getOriginalFilename() + "<br/>";
				} catch (Exception e) {
					return "You failed to upload " + multipartFile.getOriginalFilename() + ": " + e.getMessage()
							+ "<br/>";
				}
			}
			return msg;
		} else {
			return "Unable to upload. File is empty.";
		}
	}

	@PutMapping("/api/public/v1/changeNameResource")
	public ResponseEntity<Resource> changeNameResource(@RequestBody FileInfo infoDTO) {
		String file1 = infoDTO.getFile1();
		String file2 = infoDTO.getFile2();
		Path f = Paths.get("filestorage").resolve(file1).toAbsolutePath();
		System.out.println("File" + f);
		String ext = FilenameUtils.getExtension(f.toString()); 
		Path rF = Paths.get("filestorage").resolve(file2+"."+ext).toAbsolutePath();
		System.out.println("File1" + rF);
		try {
			Files.move(f, rF, StandardCopyOption.REPLACE_EXISTING);
			System.out.println("File was successfully renamed");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error: Unable to rename file");
		}
		return null;
	}
	

	@DeleteMapping("/api/public/v1/deleteNameResource/{nombreFoto:.+}")
	public ResponseEntity<Resource> deleteResource(@PathVariable String nombreFoto) {
		Path f = Paths.get("filestorage").resolve(nombreFoto).toAbsolutePath();
		try {
			Files.delete(f);
			System.out.println("File was successfully removed");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error: Unable to remove file");
		}
		return null;
	}

}
