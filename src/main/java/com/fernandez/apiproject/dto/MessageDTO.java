package com.fernandez.apiproject.dto;

public class MessageDTO {
	  private String timestamp;
	  private String message;
	  private String details;
	  public MessageDTO(){}
	  
	  public MessageDTO(String string, String message) {
		  this.timestamp = string;
		    this.message = message;
	  }

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessageDTO [timestamp=");
		builder.append(timestamp);
		builder.append(", message=");
		builder.append(message);
		builder.append(", details=");
		builder.append(details);
		builder.append("]");
		return builder.toString();
	}
	  
	  
	}
