package com.fernandez.apiproject.aop.aspects;

import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.apiproject.model.LogsAPI;
import com.ibm.icu.text.SimpleDateFormat;

@Aspect
@Component
public class ControllerLoggingAspect<TimePerformance> {

	String random = randomUUID();

	@Autowired(required = true)
	private HttpServletRequest request;

	@Autowired(required = true)
	private HttpServletResponse response;

	Logger logger = LoggerFactory.getLogger(this.getClass());

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	String sCertDate = sdf.format(new Date());
	
	@Around("bean(*Controller)")
	public Object aroundController(ProceedingJoinPoint pjp) throws Throwable {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");

		Date date = new Date(System.currentTimeMillis());
		String packageName = pjp.getSignature().getDeclaringTypeName();
		String methodName = pjp.getSignature().getName();
		LogsAPI logs = new LogsAPI();
		logs.setUuid(random);
		logs.setFecha(formatter.format(date));
		logs.setClassName(methodName);
		logs.setMethodName(packageName);
		Object output = pjp.proceed();
		
		return output;

	}

	@Around("bean(*Methods)")
	public Object aroundMethods(ProceedingJoinPoint pjp) throws Throwable {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");

		Date date = new Date(System.currentTimeMillis());
		String packageName = pjp.getSignature().getDeclaringTypeName();
		String methodName = pjp.getSignature().getName();
		LogsAPI logs = new LogsAPI();
		logs.setFecha(formatter.format(date));
		logs.setUuid(random);
		logs.setClassName(methodName);
		logs.setMethodName(packageName);
		Object output = pjp.proceed();

		return output;

	}

	@Around("bean(*Service)")
	public Object aroundService(ProceedingJoinPoint pjp) throws Throwable {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");

		Date date = new Date(System.currentTimeMillis());
		String packageName = pjp.getSignature().getDeclaringTypeName();
		String methodName = pjp.getSignature().getName();
		LogsAPI logs = new LogsAPI();
		logs.setUuid(random);
		logs.setFecha(formatter.format(date));
		logs.setClassName(methodName);
		logs.setMethodName(packageName);
		Object output = pjp.proceed();
		return output;
	}

	public String randomUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

}
