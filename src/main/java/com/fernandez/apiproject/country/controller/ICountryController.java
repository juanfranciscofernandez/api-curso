package com.fernandez.apiproject.country.controller;

import org.springframework.http.ResponseEntity;

import com.fernandez.apiproject.country.dto.SummaryCountryDTO;
import com.fernandez.apiproject.country.model.CountryDTO;

public interface ICountryController {

	ResponseEntity<SummaryCountryDTO> getCountryById(Long countryId);

}
