package com.fernandez.apiproject.country.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.apiproject.country.dto.SummaryCountryDTO;
import com.fernandez.apiproject.country.summary.SummaryCountryService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api/public")
public class CountryController implements ICountryController {

	private static final Log LOG = LogFactory.getLog(CountryController.class);

	@Autowired
	private SummaryCountryService summaryCountryService;

	@GetMapping(value = "/v1/countries", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SummaryCountryDTO>> getCountries() {
		return ResponseEntity.status(HttpStatus.OK).body(summaryCountryService.getCountriesSummary());

	}

	@GetMapping(value = "/v1/country/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SummaryCountryDTO> getCountryById(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<SummaryCountryDTO>(summaryCountryService.getCountry(id), HttpStatus.OK);
	}

	@GetMapping(value = "/v1/country/alpha3Code/{alpha3Code}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SummaryCountryDTO> getCountryByAlpha3Code(
			@PathVariable(value = "alpha3Code") String alpha3Code) {
		return new ResponseEntity<SummaryCountryDTO>(summaryCountryService.getCountryByAlpha3Code(alpha3Code),
				HttpStatus.OK);
	}

	@GetMapping(value = "/v1/country/alpha2Code/{alpha2Code}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SummaryCountryDTO> getCountryByAlpha2Code(
			@PathVariable(value = "alpha2Code") String alpha2Code) {
		return new ResponseEntity<SummaryCountryDTO>(summaryCountryService.getCountryByAlpha2Code(alpha2Code),HttpStatus.OK);
	}

}
