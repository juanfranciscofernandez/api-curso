package com.fernandez.apiproject.country.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Info Country", description = "Complete data of country")
public class SummaryCountryDTO {

	private Long id;

	private String name;

	private String capital;

	@ApiModelProperty(value = "ISO 3166-1 alpha-2 codes are two-letter country codes defined in ISO 3166-1")
	private String alpha2Code;

	@ApiModelProperty(value = "ISO 3166-1 alpha-3 codes are three-letter country codes defined in ISO 3166-1")
	private String alpha3Code;

	@ApiModelProperty(value = "ISO 3166-1 numeric (or numeric-3) codes are three-digit country codes defined in ISO 3166-1")
	private int numericCode;

	@ApiModelProperty(value = "World region ")
	private String region;

	private String flag;

	private String subregion; 

	public SummaryCountryDTO() {
		super();
	}

	public SummaryCountryDTO(Long id, String name, String capital, String alpha2Code, String alpha3Code,
			int numericCode, String region, String flag, String subregion) {
		super();
		this.id = id;
		this.name = name;
		this.capital = capital;
		this.alpha2Code = alpha2Code;
		this.alpha3Code = alpha3Code;
		this.numericCode = numericCode;
		this.region = region;
		this.flag = flag;
		this.subregion = subregion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getAlpha2Code() {
		return alpha2Code;
	}

	public void setAlpha2Code(String alpha2Code) {
		this.alpha2Code = alpha2Code;
	}

	public String getAlpha3Code() {
		return alpha3Code;
	}

	public void setAlpha3Code(String alpha3Code) {
		this.alpha3Code = alpha3Code;
	}

	public int getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(int numericCode) {
		this.numericCode = numericCode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getSubregion() {
		return subregion;
	}

	public void setSubregion(String subregion) {
		this.subregion = subregion;
	}

	@Override
	public String toString() {
		return "CountryDTO [id=" + id + ", name=" + name + ", capital=" + capital + ", alpha2Code=" + alpha2Code
				+ ", alpha3Code=" + alpha3Code + ", numericCode=" + numericCode + ", region=" + region + ", flag="
				+ flag + ", subregion=" + subregion + "]";
	}

}
