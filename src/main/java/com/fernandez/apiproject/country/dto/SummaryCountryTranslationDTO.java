package com.fernandez.apiproject.country.dto;

public class SummaryCountryTranslationDTO {

	private String nombre;

	private String capital;

	private Long languageId;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public Long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	@Override
	public String toString() {
		return "CountryTranslationDTO [nombre=" + nombre + ", capital=" + capital + ", languageId=" + languageId + "]";
	}

}
