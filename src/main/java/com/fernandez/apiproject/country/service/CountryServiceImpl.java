package com.fernandez.apiproject.country.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.apiproject.country.adapter.CountryAdapter;
import com.fernandez.apiproject.country.model.CountryDTO;
import com.fernandez.apiproject.country.repository.CountryRepository;
import com.fernandez.apiproject.model.Country;

@Component
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryRepository countryRepository;
	
	
	@Autowired
	private CountryAdapter countryAdapter;

	@Override
	public CountryDTO getCountry(Long id) {

		Country country = countryRepository.findOne(id);
		if (country != null) {
			return countryAdapter.convert2DTO(country);
		} else {
			throw new EntityNotFoundException("Country with id " + id.toString() + " not found in the database");
		}
	}
	
	@Override
	public List<CountryDTO> getAllCountries() {
		List<Country> countryList = countryRepository.findAll();	
		return countryAdapter.convertList2DTO(countryList);
	}


	@Override
	public CountryDTO getCountryByAlpha2Code(String alpha2Code) {

		Country country = countryRepository.getCountryByAlpha2Code(alpha2Code);

		if (country != null) {	
			return countryAdapter.convert2DTO(country);
		} else {
			throw new EntityNotFoundException("Country with id " + alpha2Code + " not found in the database");
		}

	}

	@Override
	public CountryDTO getCountryByAlpha3Code(String alpha3Code) {

		Country country = countryRepository.getCountryByAlpha3Code(alpha3Code);

		if (country != null) {
			return countryAdapter.convert2DTO(country);
		} else {
			throw new EntityNotFoundException("Country with id " + alpha3Code + " not found in the database");
		}

	}

	

}
