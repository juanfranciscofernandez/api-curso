package com.fernandez.apiproject.country.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.apiproject.country.adapter.CountryTranslationAdapter;
import com.fernandez.apiproject.country.model.CountryTranslationDTO;
import com.fernandez.apiproject.country.repository.CountryTranslationRepository;
import com.fernandez.apiproject.model.CountryTranslation;
import com.fernandez.apiproject.utils.language.LanguageUtils;

@Component
public class CountryTranslationServiceImpl implements CountryTranslationService {

	@Autowired
	private CountryTranslationRepository countryTranslationRepository;

	@Autowired
	private CountryTranslationAdapter countryTranslationAdapter;

	@Autowired
	private LanguageUtils languageUtils;

	@Override
	public CountryTranslationDTO getCountryTranslationById(Long countryId) {
		CountryTranslation countryTranslation = countryTranslationRepository.retreiveCountryByLanguage(countryId,languageUtils.getLanguage());
		if (countryTranslation == null) {
			return countryTranslationAdapter.convert2DTO(countryTranslationRepository.retreiveCountryByLanguage(countryId, Long.valueOf(2)));
		}
		return countryTranslationAdapter.convert2DTO(countryTranslation);
	}
	
	@Override
	public List<CountryTranslationDTO> getCountriesTranslation() {
		List<CountryTranslation> countryTranslation = countryTranslationRepository.retreiveCountriesFilteredByLanguage(languageUtils.getLanguage());
		return countryTranslationAdapter.convert2DTO(countryTranslation);
	}

}
