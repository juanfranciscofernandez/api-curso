package com.fernandez.apiproject.country.service;

import java.util.List;

import com.fernandez.apiproject.country.model.CountryTranslationDTO;

public interface CountryTranslationService {

	CountryTranslationDTO getCountryTranslationById(Long countryId);

	List<CountryTranslationDTO> getCountriesTranslation();

}
