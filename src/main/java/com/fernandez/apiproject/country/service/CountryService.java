package com.fernandez.apiproject.country.service;

import java.util.List;

import com.fernandez.apiproject.country.model.CountryDTO;

public interface CountryService {

	CountryDTO getCountry(Long id);

	CountryDTO getCountryByAlpha2Code(String alpha2Code);

	CountryDTO getCountryByAlpha3Code(String alpha3Code);

	List<CountryDTO> getAllCountries();

	

}
