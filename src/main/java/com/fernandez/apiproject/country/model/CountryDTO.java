package com.fernandez.apiproject.country.model;

public class CountryDTO {

	private Long id;

	private String alpha2Code;

	private String alpha3Code;

	private int numericCode;
	
	private String flag;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAlpha2Code() {
		return alpha2Code;
	}

	public void setAlpha2Code(String alpha2Code) {
		this.alpha2Code = alpha2Code;
	}

	public String getAlpha3Code() {
		return alpha3Code;
	}

	public void setAlpha3Code(String alpha3Code) {
		this.alpha3Code = alpha3Code;
	}

	public int getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(int numericCode) {
		this.numericCode = numericCode;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "CountryDTO [id=" + id + ", alpha2Code=" + alpha2Code + ", alpha3Code=" + alpha3Code + ", numericCode="
				+ numericCode + ", flag=" + flag + "]";
	}

	


	
	

}
