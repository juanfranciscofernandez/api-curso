package com.fernandez.apiproject.country.model;

public class CountryTranslationDTO {
	
	
	
	private String nombre;

	private String capital;

	private Long languageId;

	private Long countryId;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public Long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	public Long getCountryID() {
		return countryId;
	}

	public void setCountryID(Long countryId) {
		this.countryId = countryId;
	}

	@Override
	public String toString() {
		return "CountryTranslationDTO [nombre=" + nombre + ", capital=" + capital + ", languageId=" + languageId
				+ ", countryId=" + countryId + "]";
	}

	

}
