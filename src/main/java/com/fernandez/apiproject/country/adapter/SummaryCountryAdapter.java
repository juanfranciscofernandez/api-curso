package com.fernandez.apiproject.country.adapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fernandez.apiproject.country.dto.SummaryCountryDTO;
import com.fernandez.apiproject.country.model.CountryDTO;
import com.fernandez.apiproject.country.model.CountryTranslationDTO;

@Component
public class SummaryCountryAdapter {

	public List<SummaryCountryDTO> convertList2DTO(
			HashMap<List<CountryDTO>, List<CountryTranslationDTO>> countriesTranslationDTO2) {

		List<SummaryCountryDTO> summaryCountryDTOList = new ArrayList<SummaryCountryDTO>();

		int contar = count(countriesTranslationDTO2);

		for (int i = 0; i < contar; i++) {

			SummaryCountryDTO summaryCountryDTO = new SummaryCountryDTO();

			for (Map.Entry<List<CountryDTO>, List<CountryTranslationDTO>> entry : countriesTranslationDTO2.entrySet()) {
				summaryCountryDTO.setId(entry.getKey().get(i).getId());
				summaryCountryDTO.setName(entry.getValue().get(i).getNombre());
				summaryCountryDTO.setCapital(entry.getValue().get(i).getCapital());
				summaryCountryDTO.setNumericCode(entry.getKey().get(i).getNumericCode());
				summaryCountryDTO.setFlag(entry.getKey().get(i).getFlag());
			}

			summaryCountryDTOList.add(summaryCountryDTO);

		}
		return summaryCountryDTOList;
	}

	public static <K, V> int count(Map<K, ? extends Collection<V>> map) {
		int count = 0;
		for (Collection<V> coll : map.values())
			count += coll.size();
		return count;
	}

	public SummaryCountryDTO convert2DTO(CountryDTO countryDTO, CountryTranslationDTO countryTranslationDTO) {
		SummaryCountryDTO summaryCountryDTO = new SummaryCountryDTO();
		summaryCountryDTO.setId(countryDTO.getId());
		summaryCountryDTO.setAlpha2Code(countryDTO.getAlpha2Code());
		summaryCountryDTO.setAlpha3Code(countryDTO.getAlpha3Code());
		summaryCountryDTO.setName(countryTranslationDTO.getNombre());
		summaryCountryDTO.setCapital(countryTranslationDTO.getCapital());
		summaryCountryDTO.setNumericCode(countryDTO.getNumericCode());
		summaryCountryDTO.setFlag(countryDTO.getFlag());
		return summaryCountryDTO;
	}

}
