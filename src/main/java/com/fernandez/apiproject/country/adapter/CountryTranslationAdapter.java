package com.fernandez.apiproject.country.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fernandez.apiproject.country.model.CountryTranslationDTO;
import com.fernandez.apiproject.model.CountryTranslation;

@Component
public class CountryTranslationAdapter {

	public List<CountryTranslationDTO> convert2DTO(List<CountryTranslation> countryTranslationList) {
		List<CountryTranslationDTO> countryTranslationDTOList = new ArrayList<CountryTranslationDTO>();
		for (CountryTranslation countryTranslation : countryTranslationList) {
			countryTranslationDTOList.add(convert2DTO(countryTranslation));
		}
		return countryTranslationDTOList;
	}

	public CountryTranslationDTO convert2DTO(CountryTranslation countryTranslation) {
		CountryTranslationDTO countryTranslationDTO = new CountryTranslationDTO();
		countryTranslationDTO.setNombre(countryTranslation.getNombre());
		countryTranslationDTO.setCapital(countryTranslation.getCapital());
		countryTranslationDTO.setCountryID(countryTranslation.getCountry_id());
		countryTranslationDTO.setLanguageId(countryTranslation.getLanguageId());
		return countryTranslationDTO;
	}

}
