package com.fernandez.apiproject.country.adapter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.fernandez.apiproject.country.model.CountryDTO;
import com.fernandez.apiproject.model.Country;

@Component
public class CountryAdapter {
	
	private static final Log logger = LogFactory.getLog(CountryAdapter.class);

	public CountryDTO convert2DTO(Country country) {
		CountryDTO countryDTO = new CountryDTO();
		countryDTO.setId(country.getId());
		countryDTO.setAlpha2Code(country.getAlpha2Code());
		countryDTO.setAlpha3Code(country.getAlpha3Code());
		countryDTO.setNumericCode(country.getIsoNumeric());
		countryDTO.setFlag(country.getFlag());
		return countryDTO;
	}


	public List<CountryDTO> convertList2DTO(List<Country> countryList) {
		List<CountryDTO> countryDTOList = new ArrayList<CountryDTO>();
		for (Country country : countryList) {
			CountryDTO countryDTO = new CountryDTO();
			countryDTO.setId(country.getId());
			countryDTO.setAlpha2Code(country.getAlpha2Code());
			countryDTO.setAlpha3Code(country.getAlpha3Code());
			countryDTO.setNumericCode(country.getIsoNumeric());
			countryDTOList.add(countryDTO);
		}
		return countryDTOList;
	}

	
}
