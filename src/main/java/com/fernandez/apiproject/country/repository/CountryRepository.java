package com.fernandez.apiproject.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fernandez.apiproject.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
	
	@Query(value = "SELECT * FROM country where country.alpha3Code=:alpha3Code", nativeQuery = true)
	Country getCountryByAlpha3Code(@Param("alpha3Code") String alpha3Code);
	
	@Query(value = "SELECT * FROM country where country.alpha2Code=:alpha2Code", nativeQuery = true)
	Country getCountryByAlpha2Code(@Param("alpha2Code") String alpha2Code);	
	
	
}
