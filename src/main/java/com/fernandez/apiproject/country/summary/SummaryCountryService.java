package com.fernandez.apiproject.country.summary;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.apiproject.country.adapter.SummaryCountryAdapter;
import com.fernandez.apiproject.country.dto.SummaryCountryDTO;
import com.fernandez.apiproject.country.model.CountryDTO;
import com.fernandez.apiproject.country.model.CountryTranslationDTO;
import com.fernandez.apiproject.country.service.CountryServiceImpl;
import com.fernandez.apiproject.country.service.CountryTranslationServiceImpl;

@Component
public class SummaryCountryService {

	@Autowired
	private CountryServiceImpl countryService;

	@Autowired
	private CountryTranslationServiceImpl countryTranslationServiceImpl;

	@Autowired
	private SummaryCountryAdapter summaryCountryAdapter;

	public List<SummaryCountryDTO> getCountriesSummary() {

		HashMap<List<CountryDTO>, List<CountryTranslationDTO>> countriesTranslationDTO = getCountriesTranslation();

		return summaryCountryAdapter.convertList2DTO(countriesTranslationDTO);
	}

	public HashMap<List<CountryDTO>, List<CountryTranslationDTO>> getCountriesTranslation() {

		HashMap<List<CountryDTO>, List<CountryTranslationDTO>> countries = new HashMap<List<CountryDTO>, List<CountryTranslationDTO>>();

		List<CountryTranslationDTO> countryTranslationDTOList = getAllCountriesTranslation();

		List<CountryDTO> countryDTOList = getAllCountries();

		// Comparar dos listas
		Set<Long> ids = countryTranslationDTOList.stream().map(CountryTranslationDTO::getCountryID)
				.collect(Collectors.toSet());

		List<CountryDTO> parentBooks = countryDTOList.stream().filter(book -> !ids.contains(book.getId()))
				.collect(Collectors.toList());
		// Miramos las que faltan
		for (CountryDTO countryDTO : parentBooks) {
			countryTranslationDTOList.add(getCountryTranslationById(countryDTO.getId()));
		}

		countries.put(countryDTOList, countryTranslationDTOList);

		return countries;
	}

	public List<CountryTranslationDTO> getAllCountriesTranslation() {
		return countryTranslationServiceImpl.getCountriesTranslation();
	}

	public List<CountryDTO> getAllCountries() {
		return countryService.getAllCountries();
	}

	public SummaryCountryDTO getCountry(Long id) {
		CountryDTO countryDTO = countryService.getCountry(id);
		CountryTranslationDTO countryTranslationDTO = getCountryTranslationById(countryDTO.getId());
		return summaryCountryAdapter.convert2DTO(countryDTO, countryTranslationDTO);
	}

	public SummaryCountryDTO getCountryByAlpha3Code(String alpha3Code) {
		CountryDTO countryDTO = countryService.getCountryByAlpha3Code(alpha3Code);
		CountryTranslationDTO countryTranslationDTO = getCountryTranslationById(countryDTO.getId());
		return summaryCountryAdapter.convert2DTO(countryDTO, countryTranslationDTO);
	}

	public SummaryCountryDTO getCountryByAlpha2Code(String alpha2Code) {
		CountryDTO countryDTO = countryService.getCountryByAlpha2Code(alpha2Code);
		CountryTranslationDTO countryTranslationDTO = getCountryTranslationById(countryDTO.getId());
		return summaryCountryAdapter.convert2DTO(countryDTO, countryTranslationDTO);
	}

	public CountryTranslationDTO getCountryTranslationById(Long countryId) {
		return countryTranslationServiceImpl.getCountryTranslationById(countryId);
	}

}
