package com.fernandez.apiproject.language.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.apiproject.language.repository.LanguageRepository;
import com.fernandez.apiproject.model.Language;
import com.fernandez.apiproject.utils.generic.GenericMethods;

@Component
public class LanguageService {

	@Autowired
	private LanguageRepository languageRepository;
	
	@Autowired
	private GenericMethods genericMethods;
	
	public List<Language> findAllLanguages() {
		List<Language> language = languageRepository.findAll();
		return language;
	}
	
	public Language findLanguageByKey(String languageKey) {
		Language language = languageRepository.findLanguageByKey(languageKey);
		return language;
	}
	
	public Language findLanguageByIso639_1(String languageKey) {
		Language language = languageRepository.findLanguageByIso639_1(languageKey);
		return language;
	}
	
	
	public void addLanguage(Language language) {
		languageRepository.save(language);
	}
	
	public long retreiveLastLanguage() {
		return languageRepository.findLastId();
	}
	
	
	
	
}
