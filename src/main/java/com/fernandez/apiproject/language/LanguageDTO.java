package com.fernandez.apiproject.language;

public class LanguageDTO {

	private long languageId;
	private String clave;
	private String description;
	private String iso639_1;
	private String iso639_2;
	private String name;
	private String nativeName;

	public long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIso639_1() {
		return iso639_1;
	}

	public void setIso639_1(String iso639_1) {
		this.iso639_1 = iso639_1;
	}

	public String getIso639_2() {
		return iso639_2;
	}

	public void setIso639_2(String iso639_2) {
		this.iso639_2 = iso639_2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNativeName() {
		return nativeName;
	}

	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}

	@Override
	public String toString() {
		return String.format(
				"LanguageDTO [languageId=%s, clave=%s, description=%s, iso639_1=%s, iso639_2=%s, name=%s, nativeName=%s]",
				languageId, clave, description, iso639_1, iso639_2, name, nativeName);
	}

}
