package com.fernandez.apiproject.language.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fernandez.apiproject.model.Language;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Long> {
	@Query(value = "SELECT * FROM language where language.clave=:languageId", nativeQuery = true)
	Language findLanguageByKey(@Param("languageId") String languageId);
	
	@Query(value = "SELECT MAX(language_id) FROM language", nativeQuery = true)
	long findLastId();

	@Query(value = "SELECT * FROM language where language.iso639_1=:languageKey", nativeQuery = true)
	Language findLanguageByIso639_1(@Param("languageKey") String languageKey);
}
