package com.fernandez.apiproject.language;

public class LanguagesDTO {
	private Long id;
	private String iso639_1;
	private String iso639_2;
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIso639_1() {
		return iso639_1;
	}

	public void setIso639_1(String iso639_1) {
		this.iso639_1 = iso639_1;
	}

	public String getIso639_2() {
		return iso639_2;
	}

	public void setIso639_2(String iso639_2) {
		this.iso639_2 = iso639_2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("LanguagesDTO [id=%s, iso639_1=%s, iso639_2=%s, name=%s]", id, iso639_1, iso639_2, name);
	}

}
