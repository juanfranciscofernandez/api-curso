package com.fernandez.apiproject.language;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fernandez.apiproject.model.Language;

@Component
public class LanguageAdapter {
	
	
	public Language convertLanguageDTO2Language(LanguagesDTO languageDTO) {
		Language language = new Language();
		language.setLanguageId(languageDTO.getId());
		language.setName(languageDTO.getName());
		language.setIso639_1(languageDTO.getIso639_1());
		language.setIso639_2(languageDTO.getIso639_2());
		return language;
	}
	
	public List<LanguageDTO> convertLanguagesServices2DTO(List<Language> listLanguageService){
		List<LanguageDTO> listLanguage = new ArrayList<LanguageDTO>();
		for (Language language : listLanguageService) {
			listLanguage.add(convertLanguage2LanguageDTO(language));
		}
		return listLanguage;
	}
	
	public LanguageDTO convertLanguage2LanguageDTO(Language language) {
		LanguageDTO languageDTO = new LanguageDTO();
		languageDTO.setLanguageId(language.getLanguageId());
		languageDTO.setName(language.getName());
		languageDTO.setIso639_1(language.getIso639_1());
		languageDTO.setIso639_2(language.getIso639_2());
		return languageDTO;
	}
}
