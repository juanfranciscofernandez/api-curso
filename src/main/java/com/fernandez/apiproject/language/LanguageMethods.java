package com.fernandez.apiproject.language;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.apiproject.language.service.LanguageService;
import com.fernandez.apiproject.model.Language;

@Component
public class LanguageMethods {

	@Autowired
	private LanguageService languageService;

	@Autowired
	private LanguageAdapter languageAdapter;

	@Autowired
	private HttpServletRequest request;

	public List<LanguageDTO> getAllLanguages() {
		List<LanguageDTO> listLanguageDTO = languageAdapter.convertLanguagesServices2DTO(languageService.findAllLanguages());
		return listLanguageDTO;
	}

	public List<Long> getAllLanguagesId() {
		List<Language> languageList = languageService.findAllLanguages();
		List<Long> languageListId = new ArrayList<Long>();
		for (Language language : languageList) {
			languageListId.add(language.getLanguageId());
		}
		return languageListId;
	}

	public LanguageDTO getLanguageByAlphaCode(String key) {
		Language language = languageService.findLanguageByKey(key);
		LanguageDTO languageDTO = languageAdapter.convertLanguage2LanguageDTO(language);
		return languageDTO;
	}

	public Language getLanguageByIso639_1(String key) {
		Language language = languageService.findLanguageByIso639_1(key);
		return language;
	}

	public String getLanguage() {
		String language = request.getHeader("Accept-Language");
		Language languageRepo = languageService.findLanguageByKey(language);
		if (languageRepo == null) {
			language = "en-US";
		}
		return language;
	}
}
