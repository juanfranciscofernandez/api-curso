package com.fernandez.apiproject.language.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.apiproject.language.LanguageDTO;
import com.fernandez.apiproject.language.LanguageMethods;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api/public")
public class LanguageController {

	Logger logger = LoggerFactory.getLogger(LanguageController.class);

	@Autowired
	private LanguageMethods languageMethods;

	@GetMapping("/v1/languages")
	public ResponseEntity<List<LanguageDTO>> getLanguages(Pageable pageable) {
		List<LanguageDTO> sportsList = languageMethods.getAllLanguages();
		int pageNumber = pageable.getPageNumber();
		int size = pageable.getPageSize();
		PageRequest page = new PageRequest(pageNumber, size);
		int start = page.getOffset();
		int end = (start + page.getPageSize()) > sportsList.size() ? sportsList.size() : (start + page.getPageSize());
		int totalRows = sportsList.size();
		Page<LanguageDTO> sportListPaginated = new PageImpl<LanguageDTO>(sportsList.subList(start, end), page,
				totalRows);

		logger.info("---sport list  ---" + sportListPaginated);

		return new ResponseEntity<List<LanguageDTO>>(sportListPaginated.getContent(),
				(sportListPaginated == null || sportListPaginated.getContent().isEmpty()) ? HttpStatus.NO_CONTENT
						: HttpStatus.OK);
	}

	@GetMapping("/v1/language-key/{key}")
	public ResponseEntity<Long> getBazz(@PathVariable(value = "key") String key) {
		return ResponseEntity.ok(languageMethods.getLanguageByAlphaCode(key).getLanguageId());
	}
	
	

}
