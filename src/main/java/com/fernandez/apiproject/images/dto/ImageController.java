package com.fernandez.apiproject.images.dto;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.apiproject.images.Image;
import com.fernandez.apiproject.utils.base64.UtilBase64Image;

@RestController
public class ImageController {

	@RequestMapping(value = "/api/public/images/upload", method = RequestMethod.POST)
	public String uploadImage(@RequestBody Image image) {
		String name = image.getName();
		String imagePath = "C:\\client\\" + name;
		String data = UtilBase64Image.encoder(imagePath);
		Image newImage = new Image(name, data);
		String path = "C:\\server\\" + newImage.getName();
		UtilBase64Image.decoder(newImage.getData(), path);
		return "/Post Successful!";
	}

	@RequestMapping(value = "/api/public/images/upload-alternative", method = RequestMethod.POST)
	public String post(@RequestBody Image image) {
		System.out.println("/POST request with " + image.toString());
		String path = "C:\\server\\" + image.getName();
		UtilBase64Image.decoder(image.getData(), path);
		return "/Post Successful!";
	}

	@RequestMapping(value = "/api/public/images", method = RequestMethod.GET)
	public Image get() {
		String name = "lakers.png";
		System.out.println(String.format("/GET info: imageName = %s", name));
		String imagePath = "C:\\server\\" + name;
		String imageBase64 = UtilBase64Image.encoder(imagePath);

		if (imageBase64 != null) {
			Image image = new Image(name, imageBase64);
			return image;
		}
		return null;
	}
	

}