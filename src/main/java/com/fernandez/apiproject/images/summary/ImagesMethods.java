package com.fernandez.apiproject.images.summary;

import org.springframework.stereotype.Component;

import com.fernandez.apiproject.images.Image;
import com.fernandez.apiproject.utils.base64.UtilBase64Image;

@Component
public class ImagesMethods {
	
	
	public String retreivePath(String imagePath){ 
		String data = UtilBase64Image.encoder(imagePath);
		return data;
	}
	
	

	public Image convertBase64(String name) {
		 Image image = new Image(name,  UtilBase64Image.encoder("C:\\client\\atlanta2.png"));
		 return image;
	}	
	
}
